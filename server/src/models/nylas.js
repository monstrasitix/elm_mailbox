import Joi from 'joi';

class ModelNylas {

    get headerValidation() {
        const headerValidation = {
            authorization: Joi.string().required(),
        };
        return Joi.object(headerValidation).options({
            allowUnknown: true,
        });
    }

}

export default ModelNylas;