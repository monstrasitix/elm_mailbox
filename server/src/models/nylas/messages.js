import Joi from 'joi';
import ModelNylas from '../nylas';

class ModelMessages extends ModelNylas {
    get getMessages() {
        return {
            query: {
                view: Joi.string().only('id', 'count', 'expanded'),
                offset: Joi.number().positive().integer(),
                limit: Joi.number().positive().integer(),
                any_email: Joi.string().email(),
                received_before: Joi.string(),
                received_after: Joi.string(),
                from: Joi.string().email(),
                bcc: Joi.string().email(),
                cc: Joi.string().email(),
                to: Joi.string().email(),
                thread_id: Joi.string(),
                subject: Joi.string(),
                filename: Joi.bool(),
                starred: Joi.bool(),
                unread: Joi.bool(),
                in: Joi.string(),
            },
            headers: this.headerValidation,
        };
    }

    get getMessage() {
        return {
            params: {
                id: Joi.string().required(),
            },
            headers: this.headerValidation,
        };
    }

    get putMessage() {
        return {
            params: {
                id: Joi.string().required(),
            },
            payload: {
                unread: Joi.bool(),
                starred: Joi.bool(),
                folder_id: Joi.string(),
                label_ids: Joi.string(),
            },
            headers: this.headerValidation,
        };
    }
}

export default new ModelMessages;