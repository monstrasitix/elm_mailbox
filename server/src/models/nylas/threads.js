import Joi from 'joi';
import ModelNylas from '../nylas';

class ModelThreads extends ModelNylas {

    get getThreads() {
        return {
            query: {
                last_message_before: Joi.number().positive().integer(),
                last_message_after: Joi.number().positive().integer(),
                view: Joi.string().only('id', 'count', 'expanded'),
                started_before: Joi.number().positive().integer(),
                started_after: Joi.number().positive().integer(),
                offset: Joi.number().positive().integer(),
                limit: Joi.number().positive().integer(),
                any_email: Joi.string().email(),
                from: Joi.string().email(),
                bcc: Joi.string().email(),
                to: Joi.string().email(),
                cc: Joi.string().email(),
                subject: Joi.string(),
                filename: Joi.bool(),
                starred: Joi.bool(),
                unread: Joi.bool(),
                in: Joi.string(),
            },
        };
    }

    get getThread() {
        return {
            params: {
                id: Joi.string().required(),
            },
            headers: this.headerValidation,
        };
    }

    get putThread() {
        return {
            params: {
                id: Joi.string().required(),
            },
            payload: {
                label_ids: Joi.array().items(Joi.string()),
                folder_id: Joi.string(),
                starred: Joi.bool(),
                unread: Joi.bool(),
            },
            headers: this.headerValidation,
        };
    }

}

export default new ModelThreads;