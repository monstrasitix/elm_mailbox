import routesAuthentication from './nylas/authentication';
import routesMessages from './nylas/messages';
import routesThreads from './nylas/threads';


export default [].concat(
    routesAuthentication,
    routesMessages,
    routesThreads,
);