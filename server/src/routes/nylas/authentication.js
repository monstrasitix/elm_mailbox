import handlerNylas from '../../handlers/nylas/authentication';
import { defineRoute } from '../../utils/routes';
import configApi from '../../config/api.config';


const route = defineRoute(configApi.prefix, [
    'authentication',
    'api',
]);


export default [
    route({
        method: 'GET',
        path: '/connect',
        handler: handlerNylas.connect,
        config: {
            description: 'Redirects to Nylas authentication page',
        },
    }),
    route({
        method: 'GET',
        path: '/oauth/callback',
        handler: handlerNylas.callback,
        config: {
            description: 'Fetches Nylas generated OAuth token',
        },
    }),
];