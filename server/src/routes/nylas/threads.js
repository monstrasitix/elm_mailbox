import configApi from '../../config/api.config';
import { defineRoute } from '../../utils/routes';
import modelThreads from '../../models/nylas/threads';
import handlerThreads from '../../handlers/nylas/threads';

const route = defineRoute(configApi.prefix, [
    'threads',
    'api',
]);


export default [
    route({
        method: 'GET',
        path: '/threads',
        handler: handlerThreads.getThreads,
        config: {
            description: 'Returns a collection of threads',
            validate: modelThreads.getThreads,
        },
    }),
    route({
        method: 'GET',
        path: '/threads/{id}',
        handler: handlerThreads.getThread,
        config: {
            description: 'Returns a single thread',
            validate: modelThreads.getThread,
        },
    }),
    route({
        method: 'PUT',
        path: '/threads/{id}',
        handler: handlerThreads.putThread,
        config: {
            description: 'Modifying single thread instance',
            validate: modelThreads.putThread,
        },
    }),
];