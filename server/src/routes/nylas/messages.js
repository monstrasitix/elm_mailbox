import configApi from '../../config/api.config';
import { defineRoute } from '../../utils/routes';
import modelMessages from '../../models/nylas/messages';
import handlerMessages from '../../handlers/nylas/messages';

const route = defineRoute(`${configApi.prefix}/messages`, [
    'messages',
    'api',
]);

export default [
    route({
        method: 'GET',
        path: '',
        handler: handlerMessages.getMessages,
        config: {
            notes: 'Requires authorization header',
            description: 'Returns a collection of messages',
            validate: modelMessages.getMessages,
        },
    }),
    route({
        method: 'GET',
        path: '/{id}',
        handler: handlerMessages.getMessage,
        config: {
            notes: 'Requires authorization header',
            description: 'Returns a single message',
            validate: modelMessages.getMessage,
        },
    }),
    route({
        method: 'PUT',
        path: '/{id}',
        handler: handlerMessages.putMessage,
        config: {
            notes: [
                'Requires authorization header',
                '"accept: message/rfc822". This header allows to get the original message\'s source'
            ],
            description: 'Move message or change read/unread status',
            validate: modelMessages.putMessage,
        },
    }),
];