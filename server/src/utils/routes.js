export const defineRoute = (prefix, tags) => (routing) => {

    routing.path = prefix + routing.path;
    routing.config.tags =
        routing.config.tags
            ? [...routing.config.tags, ...tags]
            : tags;

    if (!routing.config.validate) {
        routing.config.validate = {};
    }

    routing.config.validate.payload = routing.config.validate.payload || false;
    routing.config.validate.params = routing.config.validate.params || false;
    routing.config.validate.query = routing.config.validate.query || false;

    return routing;
};