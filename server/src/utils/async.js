export const replySuccess = data => ({ status: true, data });
export const replyFailure = data => ({ status: false, data });