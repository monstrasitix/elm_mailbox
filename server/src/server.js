// Core
import Hapi from 'hapi';
import Inert from 'inert';
import Vision from 'vision';
import HapiSwagger from 'hapi-swagger';
import CorsHeaders from 'hapi-cors-headers';
// Initialization
import Routes from './routes';


const Swagger = {
    register: HapiSwagger,
    options: {
        info: {
            title: 'Mailbox API Documentation',
            version: 'v1.0'
        },
    },
};


const server = new Hapi.Server();

server.connection({
    host: 'localhost',
    port: 8000,
});


server.ext('onPreResponse', CorsHeaders)


server.register([
    Inert,
    Vision,
    Swagger,
], (registrationError) => {
    if (registrationError) {
        console.log(registrationError);
    }


    server.route(Routes);


    server.start((initializationError) => {
        if (initializationError) {
            console.log(initializationError);
        }

        console.log('Server running at:', server.info.uri);
    });
});