// Libs
import { NylasInstance } from '../../libs/nylas';
// Utils
import { replyFailure, replySuccess } from '../../utils/async';


const handler = {};


handler.getMessages = async (request, reply) => {
    try {
        const messages = await NylasInstance.messages.list(request.query);
        reply(replySuccess(messages));
    } catch(requestError) {
        reply(replyFailure(requestError));
    }
};


handler.getMessage = async (request, reply) => {
    try {
        const message = await NylasInstance.messages.find(request.params.id);
        reply(replySuccess(message));
    } catch(requestError) {
        reply(replyFailure(requestError));
    }
};


handler.putMessage = async (request, reply) => {
    try {
        const message = await NylasInstance.messages.find(request.params.id);
        await message.save();
        reply(replySuccess(message));
    } catch(requestError) {
        reply(replyFailure(requestError));
    }
};


export default handler;