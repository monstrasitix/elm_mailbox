// Libs
import { NylasInstance } from '../../libs/nylas';
// Utils
import { replyFailure, replySuccess } from '../../utils/async';


const handler = {};


handler.getThreads = async (request, reply) => {
    try {
        const threads = await NylasInstance.threads.list(request.query);
        reply(replySuccess(threads));
    } catch (requestError) {
        reply(replyFailure(requestError));
    }
};


handler.getThread = async (request, reply) => {
    try {
        const thread = await NylasInstance.threads.find(request.params.id);
        reply(replySuccess(thread));
    } catch (requestError) {
        reply(replyFailure(requestError));
    }
};


handler.putThread = async (request, reply) => {
    try {
        const thread = await NylasInstance.threads.find(request.params.id);
        await thread.save();
        reply(replySuccess(thread));
    } catch (requestError) {
        reply(replyFailure(requestError));
    }
};




export default handler;