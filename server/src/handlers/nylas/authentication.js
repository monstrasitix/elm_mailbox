// Libs
import { NylasConfig } from '../../libs/nylas';
// Utils
import { replyFailure, replySuccess } from '../../utils/async';


const handler = {};


handler.connect = (request, reply) => {
    reply.redirect(NylasConfig.urlForAuthentication({
        redirectURI: 'http://localhost:8000/oauth/callback',
        trial: false,
    }));
};


handler.callback = async (request, reply) => {
    if (request.query.code) {
        try {
            const responseToken = await NylasConfig.exchangeCodeForToken(request.query.code);
            reply(replySuccess(responseToken));
        } catch (exchangeError) {
            console.error(exchangeError);
            reply(replyFailure(exchangeError));
        }
    } else if (request.query.error) {
        reply(replyFailure(request.query.reason));
    }
};


export default handler;