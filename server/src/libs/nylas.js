// Core
import Nylas from 'nylas';
// Config
import configNylas from '../config/nylas.config';


export const NylasConfig = Nylas.config(configNylas);
export const NylasInstance = Nylas.with('9OqWAkz4eIKxBsYP9zvOD3sW70Nll2');