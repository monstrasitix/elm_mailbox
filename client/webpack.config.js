
const path = require('path');

module.exports = {
  entry: './src/index.js',

  output: {
    filename: 'compiled.js',
    path: __dirname + '/src',
    publicPath: '/',
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'stage-3']
        }
      }
    ],
    rules: [
      {
        test: /\.(scss|sass)$/,
        use: [{
          loader: "style-loader"
        }, {
          loader: "css-loader"
        }, {
          loader: "resolve-url-loader"
        }, {
          loader: 'sass-loader',
          options: {
            includePaths: [
              __dirname + '/src/sass'
            ]
          }
        }]
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: 'file-loader?name=[name].[ext]',
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader: 'elm-webpack-loader?verbose=true&warn=true',
      }
    ],

    noParse: /\.elm$/,
  },

  devServer: {
    inline: true,
    stats: { colors: true },
  }
};