module Model.Response exposing (..)

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipe


-- Success Model Schema


type alias Success any =
    { data : any
    , status : Bool
    }



-- Success Decoder


decodeSuccess : Decode.Decoder a -> Decode.Decoder (Success a)
decodeSuccess decoder =
    Pipe.decode Success
        |> Pipe.required "data" decoder
        |> Pipe.required "status" Decode.bool
