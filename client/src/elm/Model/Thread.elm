module Model.Thread exposing (..)

-- Core

import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline as Pipe


-- Models

import Model.Label as Label
import Model.Participant as Participant


-- TODO: Thread model requites folder validation
-- Thread Model Schema


type alias Thread =
    { id : String
    , version : Maybe Int
    , unread : Bool
    , starred : Bool
    , object : String
    , subject : String
    , snippet : String
    , account_id : String
    , draft_ids : Maybe (List String)
    , message_ids : List String
    , labels : List Label.Label
    , last_message_timestamp : Int
    , first_message_timestamp : Maybe Int
    , last_message_received_timestamp : Maybe Int
    , participants : List Participant.Participant
    }



-- Put Thread Body


type alias PutThreadBody =
    { unread : Bool
    , starred : Bool
    , folder_id : String
    , label_ids : List String
    }



-- Thread List Decoder


decodeThreadList : Decode.Decoder (List Thread)
decodeThreadList =
    Decode.list decodeThread



-- Thread Decoder


decodeThread : Decode.Decoder Thread
decodeThread =
    Pipe.decode Thread
        |> Pipe.required "id" Decode.string
        |> Pipe.optional "version" (Decode.nullable Decode.int) Nothing
        |> Pipe.required "unread" Decode.bool
        |> Pipe.required "starred" Decode.bool
        |> Pipe.required "object" Decode.string
        |> Pipe.required "subject" Decode.string
        |> Pipe.required "snippet" Decode.string
        |> Pipe.required "account_id" Decode.string
        |> Pipe.optional "draft_ids" (Decode.nullable (Decode.list Decode.string)) Nothing
        |> Pipe.required "message_ids" (Decode.list Decode.string)
        |> Pipe.required "labels" Label.decodeLabelList
        |> Pipe.required "last_message_timestamp" Decode.int
        |> Pipe.optional "first_message_timestamp" (Decode.nullable Decode.int) Nothing
        |> Pipe.optional "last_message_received_timestamp" (Decode.nullable Decode.int) Nothing
        |> Pipe.required "participants" Participant.decodeParticipantList



-- Put Thread Body Encoder


encodePutThreadBody : PutThreadBody -> Encode.Value
encodePutThreadBody putThread =
    Encode.object
        [ ( "unread", Encode.bool putThread.unread )
        , ( "starred", Encode.bool putThread.starred )
        , ( "folder_id", Encode.string putThread.folder_id )
        , ( "label_ids", Encode.list (List.map Encode.string putThread.label_ids) )
        ]
