module Model.Participant exposing (..)

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipe


-- Participant Model Schema


type alias Participant =
    { name : String
    , email : String
    , object : String
    }



-- Participant List Decoder


decodeParticipantList : Decode.Decoder (List Participant)
decodeParticipantList =
    Decode.list decodeParticipant



-- Participant Decoder


decodeParticipant : Decode.Decoder Participant
decodeParticipant =
    Pipe.decode Participant
        |> Pipe.required "name" Decode.string
        |> Pipe.required "email" Decode.string
        |> Pipe.required "object" Decode.string
