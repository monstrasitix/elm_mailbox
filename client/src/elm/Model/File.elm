module Model.File exposing (..)

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipe


-- File Model Schema


type alias File =
    { size : Int
    , id : String
    , object : String
    , filename : String
    , content_type : String
    , message_ids : List String
    , content_id : Maybe String
    }



-- File List Decoder


decodeFileList : Decode.Decoder (List File)
decodeFileList =
    Decode.list decodeFile



-- File Decpder


decodeFile : Decode.Decoder File
decodeFile =
    Pipe.decode File
        |> Pipe.required "size" Decode.int
        |> Pipe.required "id" Decode.string
        |> Pipe.required "object" Decode.string
        |> Pipe.required "filename" Decode.string
        |> Pipe.required "content_type" Decode.string
        |> Pipe.required "message_ids" (Decode.list Decode.string)
        |> Pipe.optional "content_id" (Decode.nullable Decode.string) Nothing
