module Model.Label exposing (..)

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipe


-- Label Model Schema


type alias Label =
    { id : String
    , name : String
    , object : String
    , display_name : String
    }



-- Label List Decoder


decodeLabelList : Decode.Decoder (List Label)
decodeLabelList =
    Decode.list decodeLabel



-- Label Decoder


decodeLabel : Decode.Decoder Label
decodeLabel =
    Pipe.decode Label
        |> Pipe.required "id" Decode.string
        |> Pipe.required "name" Decode.string
        |> Pipe.required "object" Decode.string
        |> Pipe.required "display_name" Decode.string
