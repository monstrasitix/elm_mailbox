module Model.Contact exposing (..)

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipe


-- Contact Model Schema


type alias Contact =
    { name : String
    , email : String
    , object : String
    }



-- Contact List Decoder


decodeContactList : Decode.Decoder (List Contact)
decodeContactList =
    Decode.list decodeContact



-- Contact Decoder


decodeContact : Decode.Decoder Contact
decodeContact =
    Pipe.decode Contact
        |> Pipe.required "name" Decode.string
        |> Pipe.required "email" Decode.string
        |> Pipe.required "object" Decode.string
