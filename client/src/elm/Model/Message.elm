module Model.Message exposing (..)

-- Core

import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline as Pipe


-- Models

import Model.File as File
import Model.Label as Label
import Model.Contact as Contact


-- TODO: Message model requires events
-- TODO: Message model requires folder
-- Message Model Schema


type alias Message =
    { date : Int
    , id : String
    , draft : Bool
    , body : String
    , unread : Bool
    , starred : Bool
    , object : String
    , snippet : String
    , subject : String
    , thread_id : String
    , account_id : String
    , files : List File.File
    , cc : List Contact.Contact
    , labels : List Label.Label
    , to : List Contact.Contact
    , bcc : List Contact.Contact
    , from : List Contact.Contact
    , reply_to : Maybe (List Contact.Contact)
    }



-- PUT Message Body Schema


type alias PutMessageBody =
    { unread : Bool
    , starred : Bool
    , folder_id : String
    , label_ids : String
    }



-- Message List Decoder


decodeMessageList : Decode.Decoder (List Message)
decodeMessageList =
    Decode.list decodeMessage



-- Message Decoder


decodeMessage : Decode.Decoder Message
decodeMessage =
    Pipe.decode Message
        |> Pipe.required "date" Decode.int
        |> Pipe.required "id" Decode.string
        |> Pipe.required "draft" Decode.bool
        |> Pipe.required "body" Decode.string
        |> Pipe.required "unread" Decode.bool
        |> Pipe.required "starred" Decode.bool
        |> Pipe.required "object" Decode.string
        |> Pipe.required "snippet" Decode.string
        |> Pipe.required "subject" Decode.string
        |> Pipe.required "thread_id" Decode.string
        |> Pipe.required "account_id" Decode.string
        |> Pipe.required "files" File.decodeFileList
        |> Pipe.required "cc" Contact.decodeContactList
        |> Pipe.required "labels" Label.decodeLabelList
        |> Pipe.required "to" Contact.decodeContactList
        |> Pipe.required "bcc" Contact.decodeContactList
        |> Pipe.required "from" Contact.decodeContactList
        |> Pipe.optional "reply_to" (Decode.nullable Contact.decodeContactList) Nothing



-- Put Message Body Encoder


encodePutMessageBody : PutMessageBody -> Encode.Value
encodePutMessageBody putMessage =
    Encode.object
        [ ( "unread", Encode.bool putMessage.unread )
        , ( "starred", Encode.bool putMessage.starred )
        , ( "folder_id", Encode.string putMessage.folder_id )
        , ( "label_ids", Encode.string putMessage.label_ids )
        ]
