module Validation.FormValidation exposing (..)
import Html
import Html.Events as Event
import Html.Attributes as Attribute
import Dict
import Debug





-- Initial Model Schema
type alias Model =
  { loginForm : Form
  }
  



-- Initial Model
model : Model
model =
  { loginForm = initialize
      [ ( "email", "", emailValidations )
      , ( "password", "", emailValidations )
      ]
  }
  
-- Validation Example  
emailValidations : List FieldValidator
emailValidations =
  [ required "This field is required"
  , minLength 5 "Minimum length is 5"
  ]

  

  
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
  
  
{-|
  @description
    Taking all defined fields and converting them into a required format for the form 
  @param { List InitialField } initFields - Field settings
  @return { Form } - Form
|-}  
initialize : List InitialField -> Form
initialize initFields =
  let
    -- Converting a list of tuples into a list of records
    fieldList : List Field
    fieldList = convertToListField initFields
     
    -- Validating field records during initialisation
    -- And changing the structure suitable for Dictionary structure
    validateFields : List (FieldName, Field)
    validateFields = convertToValidated fieldList
  in
    { fields = Dict.fromList validateFields
    , isValid = isFormValid validateFields
    }
  
  
{-|
  @description
    Going through every field an creating a field record
  @param { List InitialField } initFields - Field Settings
  @return { List Field } - Returning a list of field records
|-}
convertToListField : List InitialField -> List Field
convertToListField initFields =
  initFields
    |> List.map
      (\ (fieldName, fieldValue, validators) ->
        { name = fieldName
        , value = fieldValue
        , error = Just ""
        , errors = []
        , validators = validators
        , dirty = False
        })
        

{-|
  @description
    Creating a tuple which is suitable for the Dictionary structure. The Form model
    utilizes dictionary for ease of access.
    
    This function also validates the fields
  @param { List Field } fieldList - List of field records
  @return { List ( FieldName, Field ) } - Field record within a tuple
|-}
convertToValidated : List Field -> List (FieldName, Field)
convertToValidated fieldList =
  fieldList
    |> List.map
      (\ field ->
        let
          errorList : List FieldError
          errorList =
            validateField field
        in
          ( field.name
          , { name = field.name
            , value = field.value
            , error = getErrorFromList errorList
            , errors = errorList
            , validators = field.validators
            , dirty = initialFieldState field
            })
          )
  
{-|
  @description
    Returns an error message
  @param { Field } - Field records
  @return { List FieldError } - Returns a list of errors
|-}  
validateField : Field -> List FieldError
validateField field =
  field.validators
    |> List.map (\ validator -> validator field.value)
    |> List.filter (\ error -> error /= Nothing)
    

{-|
  @description
    Returns first error within the error list
  @param { List FieldError } - All errors within the field
  @return { FieldError } - First error withing the list
|-}
getErrorFromList : List FieldError -> FieldError
getErrorFromList errorList =
  case List.head errorList of
    Just err -> err
    Nothing -> Nothing

{-|
  @description
    I don't want an error to show up when the field is empty, even if it has an error.
    This function is only used for "dirty" flag. - Errors are still present.
  @param { Field } field - Field records
  @return { Bool } - Is the form touched or not
|-}
initialFieldState : Field -> Bool
initialFieldState field =
  if field.value == "" then
    False
  else
    case getErrorFromList <| validateField field of
      Just err -> True
      Nothing -> False


{-|
  @description
    Iterates through every field and checks does it have a error or not.
    If errors aren't present on all fields then the form is considered to be valid.
  @param { List (FieldName, Field) } fieldList - List of tuple fields
  @return { Bool } - Flag stating is the form valid or not
|-}
isFormValid : List (FieldName, Field) -> Bool
isFormValid fieldList =
  let
    isValid : List (FieldName, Field)
    isValid =
      fieldList
        |> List.filter
          (\ (fieldName, field) -> field.error /= Nothing )
  in
    List.isEmpty isValid


{-|
  @fix
    Needs an alternative to "Nothing" case.
  @description
    Gets the field specified by the name. That name will match the tupe in which
    the Field record is stored.
  @param { Form } - Form record
  @param { FieldName } - Name of the field which needs to be received
  @return { Field } - Field which was searched for
|-}
getField : Form -> FieldName -> Field
getField form fieldName =
  case Dict.get fieldName form.fields of
    Just field -> field
    Nothing -> Field "" "" (Just "") [] [] False


{-|
  @description
    Gets the error for a specified field
  @param { Field } - Field record
  @return { String } - Error message
|-}
getError : Field -> String
getError field =
  if field.dirty == True then
    case field.error of
      Just err -> err
      Nothing -> ""
  else
    ""
  
  
{-|
  @description
    Checks the error property on the field record. And returns a flag depending on
    field's validity.
  @param { Field } - Field record
  @return { Bool } - Indicates if the field is without errors
|-}  
isFieldValid : Field -> Bool
isFieldValid field =
  case field.error of
    Just err -> False
    Nothing -> True


minLength : Int -> String -> FieldValue -> FieldError
minLength length error value =
  if String.length value < length
  then Just error
  else Nothing


required : String -> FieldValue -> FieldError
required error value =
  if value == ""
  then Just error
  else Nothing
  
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    UpdateForm fieldName fieldValue ->
      let
        fields : List (FieldName, Field)
        fields =
          (Dict.toList <| model.loginForm.fields)
            |> List.map
                (\ (name, field) ->
                  if name == fieldName then
                    let
                      errorList : List FieldError
                      errorList =
                        validateField { field | value = fieldValue }
                    in
                      ( name
                      , { field
                        | error = getErrorFromList errorList
                        , errors = errorList
                        , value = fieldValue
                        , dirty = True
                        }
                    )
                  else
                    ( name, field )
                )
      in
        ( { model | loginForm = Form (Dict.fromList fields) (isFormValid fields) }
        , Cmd.none
        )
      
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}
{--------------------------------------------------------------------------------------------------------------------------}

view : Model -> Html.Html Msg
view model =
  Html.div []
    [ bootstrap "paper"
    , Html.br [][]
    , Html.br [][]
    , Html.br [][]
    , Html.br [][]
    , Html.div [ Attribute.class "container" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-8 col-xs-offset-2" ]
                [ Html.div [ Attribute.class "panel panel-default" ]
                    [ Html.div [ Attribute.class "panel-body" ]
                        [ viewForm model ]
                    ]
                ]
            ]
        ]
    ]
    

viewForm : Model -> Html.Html Msg
viewForm model =
  Html.form []
    [ emailField <| getField model.loginForm "email"
    , passwordField <| getField model.loginForm "password"
    ]

    
emailField : Field -> Html.Html Msg
emailField field =
  Html.div [ Attribute.class <| (++) "form-group col-xs-12" <| hasError field ]
    [ Html.label [ Attribute.for field.name ] [ Html.text "Email" ]
    , Html.input
        [ Attribute.id field.name
        , Attribute.name field.name
        , Attribute.type_ "text"
        , Attribute.class "form-control"
        , Attribute.placeholder "Enter your email"
        , Attribute.value field.value
        , Event.onInput <| UpdateForm field.name
        ]
        []
    , Html.span [ Attribute.class "help-block" ][ Html.text <| getError field ]
    ]
    
hasError : Field -> String
hasError field =
  if getError field == "" then
    ""
  else
    " has-error"

    
passwordField : Field -> Html.Html Msg
passwordField field =
  Html.div [ Attribute.class <| (++) "form-group col-xs-12" <| hasError field ]
    [ Html.label [ Attribute.for field.name ] [ Html.text "Password" ]
    , Html.input
        [ Attribute.id field.name
        , Attribute.name field.name
        , Attribute.type_ "password"
        , Attribute.class "form-control"
        , Attribute.placeholder "Enter your Password"
        , Attribute.value field.value
        , Event.onInput <| UpdateForm field.name
        ]
        []
    , Html.span [ Attribute.class "help-block" ][ Html.text <| getError field ]
    ]


bootstrap : String -> Html.Html msg
bootstrap theme =
  Html.node "link"
    [ Attribute.rel "stylesheet"
    , Attribute.type_ "text/css"
    , Attribute.href <| "https://bootswatch.com/" ++ theme ++ "/bootstrap.min.css"
    ]
    []
