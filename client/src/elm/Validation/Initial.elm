module Validation.Initial exposing (..)

-- Core

import Dict


-- Types

import Validation.Types as Validation


-- Validation Initialisation Function


initialize : List Validation.InitialField -> Validation.Form
initialize initFields =
    let
        fieldList : List Validation.Field
        fieldList =
            convertToListField initFields

        validateFields : List ( Validation.FieldName, Validation.Field )
        validateFields =
            convertToValidated fieldList
    in
        { fields = Dict.fromList validateFields
        , isValid = isFormValid validateFields
        }



-- Validation Convertion To List Of Fields


convertToListField : List Validation.InitialField -> List Validation.Field
convertToListField initFields =
    initFields
        |> List.map
            (\( fieldName, fieldValue, validators ) ->
                { name = fieldName
                , value = fieldValue
                , error = Just ""
                , errors = []
                , validators = validators
                , dirty = False
                }
            )


convertToValidated : List Validation.Field -> List ( Validation.FieldName, Validation.Field )
convertToValidated fieldList =
    fieldList
        |> List.map
            (\field ->
                let
                    errorList : List Validation.FieldError
                    errorList =
                        validateField field
                in
                    ( field.name
                    , { name = field.name
                      , value = field.value
                      , error = getErrorFromList errorList
                      , errors = errorList
                      , validators = field.validators
                      , dirty = initialFieldState field
                      }
                    )
            )


validateField : Validation.Field -> List Validation.FieldError
validateField field =
    field.validators
        |> List.map (\validator -> validator field.value)
        |> List.filter (\error -> error /= Nothing)


getErrorFromList : List Validation.FieldError -> Validation.FieldError
getErrorFromList errorList =
    case List.head errorList of
        Just err ->
            err

        Nothing ->
            Nothing


initialFieldState : Validation.Field -> Bool
initialFieldState field =
    if field.value == "" then
        False
    else
        case getErrorFromList <| validateField field of
            Just err ->
                True

            Nothing ->
                False


isFormValid : List ( Validation.FieldName, Validation.Field ) -> Bool
isFormValid fieldList =
    let
        isValid : List ( Validation.FieldName, Validation.Field )
        isValid =
            fieldList
                |> List.filter
                    (\( fieldName, field ) -> field.error /= Nothing)
    in
        List.isEmpty isValid


getField : Validation.Form -> Validation.FieldName -> Validation.Field
getField form fieldName =
    case Dict.get fieldName form.fields of
        Just field ->
            field

        Nothing ->
            Validation.Field "" "" (Just "") [] [] False


getError : Validation.Field -> String
getError field =
    if field.dirty == True then
        case field.error of
            Just err ->
                err

            Nothing ->
                ""
    else
        ""


isFieldValid : Validation.Field -> Bool
isFieldValid field =
    case field.error of
        Just err ->
            False

        Nothing ->
            True


minLength : Int -> String -> Validation.FieldValue -> Validation.FieldError
minLength length error value =
    if String.length value < length then
        Just error
    else
        Nothing


required : String -> Validation.FieldValue -> Validation.FieldError
required error value =
    if value == "" then
        Just error
    else
        Nothing
