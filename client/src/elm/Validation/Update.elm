module Validation.Update exposing (update)

-- Core

import Dict
import Validation.Initial as Initial


-- Types

import Validation.Types as Validation exposing (..)


-- Validation Update Funciton


update : Msg -> a -> ( a, Cmd Msg )
update msg model =
    case msg of
        UpdateForm fieldName fieldValue ->
            let
                fields : List ( FieldName, Field )
                fields =
                    (Dict.toList <| model.loginForm.fields)
                        |> List.map
                            (\( name, field ) ->
                                if name == fieldName then
                                    let
                                        errorList : List FieldError
                                        errorList =
                                            Initial.validateField { field | value = fieldValue }
                                    in
                                        ( name
                                        , { field
                                            | error = Initial.getErrorFromList errorList
                                            , errors = errorList
                                            , value = fieldValue
                                            , dirty = True
                                          }
                                        )
                                else
                                    ( name, field )
                            )
            in
                ( { model | loginForm = Form (Dict.fromList fields) (Initial.isFormValid fields) }
                , Cmd.none
                )
