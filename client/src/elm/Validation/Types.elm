module Validation.Types exposing (..)

-- Core

import Dict


-- Validation  Messages


type Msg
    = UpdateForm FieldName FieldValue



-- Validation Model Aliases


type alias FieldName =
    String


type alias FieldValue =
    String


type alias FieldError =
    Maybe String


type alias FieldValidator =
    FieldValue -> FieldError


type alias InitialField =
    ( FieldName, FieldValue, List FieldValidator )



-- Validation Field Schema


type alias Field =
    { name : FieldName
    , value : FieldValue
    , error : FieldError
    , errors : List FieldError
    , validators : List FieldValidator
    , dirty : Bool
    }



-- Validation Form Schema


type alias Form =
    { fields : Dict.Dict FieldName Field
    , isValid : Bool
    }
