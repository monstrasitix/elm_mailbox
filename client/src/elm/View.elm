module View exposing (view)

-- Core

import Html
import Routing
import Html.Attributes as Attribute
import Config.Interop exposing (config)


-- Types

import Types as Main exposing (Model, Msg)


-- Views

import Menu.View
import Error.View
import Mailbox.View
import Settings.View
import Composition.View


-- Main View Function


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ layout model
        ]



-- Main Layoyt


layout : Model -> Html.Html Msg
layout model =
    Html.div [ Attribute.class "layout" ]
        [ -- Menu View
          Html.div [ Attribute.class "layout__menu" ]
            [ Menu.View.view model.menuModel
                |> Html.map Main.MenuMessages
            ]
          -- Routing Other Views
        , Html.div
            [ Attribute.class "layout__body"
            , Attribute.id config.observableSelector
            ]
            [ routing model ]
          -- Settings View
        , Html.div
            [ Attribute.id "settingsToggle"
            , Attribute.class "layout__settings"
            ]
            [ Settings.View.view model.settingsModel
                |> Html.map Main.SettingsMessages
            ]
        ]



-- Routing Function


routing : Model -> Html.Html Msg
routing model =
    case model.route of
        -- Mailbox Routing
        Routing.Route_Mailbox ->
            Mailbox.View.view model.mailboxModel
                |> Html.map Main.MailboxMessages

        -- Composition Routing
        Routing.Route_Composition ->
            Composition.View.view model.compositionModel
                |> Html.map Main.CompositionMessages

        -- Not Found
        Routing.Route_None ->
            Error.View.notFoundRoute
