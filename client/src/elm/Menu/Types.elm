module Menu.Types exposing (..)

-- Menu Messages


type Msg
    = None



-- Menu Model Schema


type alias Model =
    { links : List Link
    , folders : List Link
    }



-- Menu Link Model


type alias Link =
    { text : String
    , icon : String
    }



-- Menu Model


model : Model
model =
    { links =
        [ Link "Inbox" "fa fa-inbox"
        , Link "Starred" "fa fa-star"
        , Link "Snoozed" "fa fa-clock-o"
        , Link "Drafts" "fa fa-pencil-square"
        , Link "Sent" "fa fa-paper-plane"
        , Link "Junk" "fa fa-thumbs-down"
        , Link "Trash" "fa fa-trash"
        ]
    , folders =
        [ Link "Family" "fa fa-folder"
        , Link "Work" "fa fa-folder"
        , Link "School" "fa fa-folder"
        , Link "YouTube" "fa fa-folder"
        ]
    }
