module Menu.Update exposing (update)

-- Types

import Menu.Types as Menu exposing (Model, Msg)


-- Menu Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    model ! []
