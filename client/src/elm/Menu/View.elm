module Menu.View exposing (view)

-- Core

import Html
import Html.Attributes as Attribute
import Menu.Types as Menu exposing (Model, Msg)


-- Menu View Function


view : Model -> Html.Html Msg
view model =
    Html.div []
        (List.concat
            [ [ menuCompositionLink
              , Html.div [ Attribute.class "clearfix" ] []
              ]
            , (menuItemStack "Mailbox" model.links)
            , (menuItemStack "Folders" model.folders)
            ]
        )



-- Menu Item Stack


menuItemStack : String -> List Menu.Link -> List (Html.Html Msg)
menuItemStack heading links =
    [ Html.br [] []
    , Html.div [ Attribute.class "container-fluid" ]
        [ Html.p [ Attribute.class "lead text-muted" ]
            [ Html.text heading ]
        ]
    , menuList links
    ]



-- Menu Composition Link


menuCompositionLink : Html.Html Msg
menuCompositionLink =
    Html.div [ Attribute.class "btn-group flex" ]
        [ Html.button
            [ Attribute.type_ "button"
            , Attribute.class "btn btn-success no-border"
            , Attribute.attribute "data-toggle" "toggle"
            , Attribute.attribute "data-target" "#settingsToggle"
            ]
            [ Html.i [ Attribute.class "fa fa-cog" ] [] ]
        , Html.a
            [ Attribute.href "#composition"
            , Attribute.class "btn btn-primary no-border btn-block"
            ]
            [ Html.i
                [ Attribute.class "fa fa-paper-plane-o menu-icon-margin" ]
                []
            , Html.text "Compose"
            ]
        ]



-- Menu List


menuList : List Menu.Link -> Html.Html Msg
menuList links =
    Html.ul [ Attribute.class "nav nav-pills nav-stacked" ]
        (List.map menuLink links)



-- Menu Link


menuLink : Menu.Link -> Html.Html Msg
menuLink link =
    let
        icon : String
        icon =
            "menu__icon " ++ link.icon
    in
        Html.li [ ]
            [ Html.a
                [ Attribute.href "#" ]
                [ Html.i [ Attribute.class icon ] []
                , Html.text " "
                , Html.text link.text
                , Html.span [ Attribute.class "label label-primary pull-right" ]
                    [ Html.text "20" ]
                ]
            ]
