port module Main exposing (..)

-- Core

import View
import Task
import Update
import Routing
import Navigation
import Config.Interop exposing (config)


-- Requests

import Request.Messages


-- Types

import Mailbox.Types
import Mailbox.List.Types
import Types as Main exposing (Model, Msg)


-- Subscriptions

import Mailbox.Subscriptions
import Composition.Subscriptions


-- Observer Inisialisation Port


port selectors : String -> Cmd message



-- Main Function


main : Program Never Model Msg
main =
    Navigation.program Main.OnLocationChange
        { init = init
        , view = View.view
        , update = Update.update
        , subscriptions = subscriptions
        }



-- Initializing


init : Navigation.Location -> ( Model, Cmd Msg )
init location =
    let
        -- Parsing The Route
        route : Routing.Route
        route =
            Routing.parseLocation location

        -- Mailbox Message For Requesting Messages
        mailboxMessage : Cmd Mailbox.Types.Msg
        mailboxMessage =
            Cmd.map Mailbox.Types.ListMessages <|
                Task.attempt Mailbox.List.Types.ResponseMessages <|
                    Request.Messages.getMessages [ ( "limit", "50" ) ]
    in
        (!)
            (Main.model route)
            [ -- Initializint DOM Selectors
              selectors config.observableSelector
              -- Fetching Messages
            , Cmd.map Main.MailboxMessages mailboxMessage
            ]



-- Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ -- Subscribing Composition Editor Messages
          Composition.Subscriptions.subscriptions model.compositionModel
            |> Sub.map Main.CompositionMessages
          -- Subscribing Mailbox Preview Reply Editor Messages
        , Mailbox.Subscriptions.subscriptions model.mailboxModel
            |> Sub.map Main.MailboxMessages
        ]
