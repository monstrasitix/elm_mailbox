module Routing exposing (..)

-- Core

import UrlParser
import Navigation


-- Routes


type Route
    = Route_None
    | Route_Mailbox
    | Route_Composition



-- Route Matching


matchers : UrlParser.Parser (Route -> a) a
matchers =
    UrlParser.oneOf
        [ UrlParser.map Route_Mailbox UrlParser.top
        , UrlParser.map Route_Composition (UrlParser.s "composition")
        ]



-- Location Parsing To Route


parseLocation : Navigation.Location -> Route
parseLocation location =
    let
        parsedUrl : Maybe Route
        parsedUrl =
            UrlParser.parseHash matchers location
    in
        case parsedUrl of
            -- Route Parsed
            Just route ->
                route

            -- No Route
            Nothing ->
                Route_None
