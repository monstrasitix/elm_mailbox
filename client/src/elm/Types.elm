module Types exposing (..)

-- Core

import Routing
import Navigation


-- Types

import Menu.Types
import Mailbox.Types
import Settings.Types
import Composition.Types


-- Main Messages


type Msg
    = MenuMessages Menu.Types.Msg
    | MailboxMessages Mailbox.Types.Msg
    | SettingsMessages Settings.Types.Msg
    | OnLocationChange Navigation.Location
    | CompositionMessages Composition.Types.Msg



-- Main Model Schema


type alias Model =
    { route : Routing.Route
    , menuModel : Menu.Types.Model
    , mailboxModel : Mailbox.Types.Model
    , settingsModel : Settings.Types.Model
    , compositionModel : Composition.Types.Model
    }



-- Main Model


model : Routing.Route -> Model
model route =
    { route = route
    , menuModel = Menu.Types.model
    , mailboxModel = Mailbox.Types.model
    , settingsModel = Settings.Types.model
    , compositionModel = Composition.Types.model
    }
