module Update exposing (update)

-- Core

import Routing


-- Types

import Types as Main exposing (Model, Msg)


-- Updates

import Menu.Update
import Mailbox.Update
import Settings.Update
import Composition.Update


-- Main Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Parse The Route When Location Is Changed
        Main.OnLocationChange location ->
            let
                newRoute : Routing.Route
                newRoute =
                    Routing.parseLocation location
            in
                (!)
                    { model | route = newRoute }
                    []

        -- Binding Menu Messages
        Main.MenuMessages message ->
            let
                ( newModel, newCmd ) =
                    Menu.Update.update message model.menuModel
            in
                (!)
                    { model | menuModel = newModel }
                    [ Cmd.map Main.MenuMessages newCmd ]

        -- Binding Mailbox Messages
        Main.MailboxMessages message ->
            let
                ( newModel, newCmd ) =
                    Mailbox.Update.update message model.mailboxModel
            in
                (!)
                    { model | mailboxModel = newModel }
                    [ Cmd.map Main.MailboxMessages newCmd ]

        -- Binding Composition Messages
        Main.CompositionMessages message ->
            let
                ( newModel, newCmd ) =
                    Composition.Update.update message model.compositionModel
            in
                (!)
                    { model | compositionModel = newModel }
                    [ Cmd.map Main.CompositionMessages newCmd ]

        -- Binding Settings Messages
        Main.SettingsMessages message ->
            let
                ( newModel, newCmd ) =
                    Settings.Update.update message model.settingsModel
            in
                (!)
                    { model | settingsModel = newModel }
                    [ Cmd.map Main.SettingsMessages newCmd ]
