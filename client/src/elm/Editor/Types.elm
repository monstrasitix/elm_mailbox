module Editor.Types exposing (..)

-- Editor Messages


type Msg
    = ReceiveValue String



-- Editor Model Schema


type alias Model =
    { value : String
    }



-- Editor Model


model : Model
model =
    { value = ""
    }
