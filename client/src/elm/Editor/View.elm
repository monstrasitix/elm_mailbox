module Editor.View exposing (..)

-- Core

import Html
import Html.Attributes as Attribute


-- Types

import Editor.Types as Editor exposing (Model, Msg)


-- Editor View Function


view : List (Html.Attribute Msg) -> Model -> Html.Html Msg
view attrs model =
    Html.div attrs []



-- Editor Footer


footer : Html.Html Msg
footer =
    Html.div [ Attribute.class "panel-footer" ]
        [ Html.div [ Attribute.class "pull-right" ]
            [ Html.button
                [ Attribute.type_ "button"
                , Attribute.class "btn btn-sm btn-primary"
                ]
                [ Html.text "Clear" ]
            , Html.text " "
            , Html.button
                [ Attribute.type_ "button"
                , Attribute.class "btn btn-sm btn-success"
                ]
                [ Html.text "Send" ]
            ]
        ]
