module Editor.Update exposing (update)

-- Types

import Editor.Types as Editor exposing (Model, Msg)


-- Editor Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Receives Value From The Editor
        Editor.ReceiveValue newHtml ->
            (!)
                (model
                    |> updateValue newHtml
                )
                []


updateValue : String -> Model -> Model
updateValue value model =
    { model | value = value }
