port module Editor.Subscriptions exposing (..)

-- Types

import Editor.Types as Editor exposing (Model, Msg)


-- Editor Ports


port editorReceiveValue : (String -> msg) -> Sub msg



-- Editor Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    editorReceiveValue Editor.ReceiveValue
