module Helper.Chevron exposing (..)

import Html
import Html.Attributes as Attribute


-- Chevron Button


chevronButton : Html.Html msg
chevronButton =
    Html.button
        [ Attribute.type_ "button"
        , Attribute.class "btn btn-xs btn-primary pull-right"
        ]
        [ Html.i [ Attribute.class "fa fa-angle-right" ] [] ]
