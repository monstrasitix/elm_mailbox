module Helper.Progress exposing (..)

import Html
import Html.Attributes as Attribute


-- Progress Bar


progressBar : Int -> Html.Html msg
progressBar percentage =
    Html.div [ Attribute.class "progress progress-striped active" ]
        [ Html.div
            [ Attribute.class "progress-bar progress-bar-success"
            , Attribute.style [ ( "width", (++) (toString percentage) "%" ) ]
            ]
            []
        ]
