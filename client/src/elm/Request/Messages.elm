module Request.Messages exposing (..)

-- Core

import Http
import Task
import HttpBuilder
import Config.Api exposing (config)


-- Models

import Model.Message as Message
import Model.Response as Response


-- Gets Message List


getMessages : List ( String, String ) -> Task.Task Http.Error (Response.Success (List Message.Message))
getMessages queryString =
    HttpBuilder.get (config.uri ++ "/messages")
        |> HttpBuilder.withQueryParams queryString
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Message.decodeMessageList)
        |> HttpBuilder.toTask



-- Gets Message


getMessage : String -> Task.Task Http.Error (Response.Success Message.Message)
getMessage id =
    HttpBuilder.get (config.uri ++ "/messages/" ++ id)
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Message.decodeMessage)
        |> HttpBuilder.toTask



-- TODO: Message PUT request needs a response validation
-- Modifies Message


putMessage : String -> Message.PutMessageBody -> Task.Task Http.Error (Response.Success Message.Message)
putMessage id body =
    HttpBuilder.put (config.uri ++ "/messages/" ++ id)
        |> HttpBuilder.withBody
            (Http.jsonBody <| Message.encodePutMessageBody body)
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Message.decodeMessage)
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.toTask
