module Request.Threads exposing (..)

-- Core

import Http
import Task
import HttpBuilder
import Config.Api exposing (config)


-- Models

import Model.Thread as Thread
import Model.Response as Response


-- Gets Thread List


getThreads : List ( String, String ) -> Task.Task Http.Error (Response.Success (List Thread.Thread))
getThreads queryString =
    HttpBuilder.get (config.uri ++ "/threads")
        |> HttpBuilder.withQueryParams queryString
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Thread.decodeThreadList)
        |> HttpBuilder.toTask



-- Gets Thread


getThread : String -> Task.Task Http.Error (Response.Success Thread.Thread)
getThread id =
    HttpBuilder.get (config.uri ++ "/threads/" ++ id)
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Thread.decodeThread)
        |> HttpBuilder.toTask



-- Modyfies Thread
-- TODO: Threads PUT request needs response validation


putThread : String -> Thread.PutThreadBody -> Task.Task Http.Error (Response.Success Thread.Thread)
putThread id body =
    HttpBuilder.put (config.uri ++ "/threads/" ++ id)
        |> HttpBuilder.withBody
            (Http.jsonBody <| Thread.encodePutThreadBody body)
        |> HttpBuilder.withExpect
            (Http.expectJson <| Response.decodeSuccess Thread.decodeThread)
        |> HttpBuilder.withHeader "Authorization" config.nylasToken
        |> HttpBuilder.toTask
