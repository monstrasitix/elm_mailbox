module Settings.Update exposing (update)

-- Types

import Settings.Types as Settings exposing (Model, Msg)


-- Settings Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    model ! []
