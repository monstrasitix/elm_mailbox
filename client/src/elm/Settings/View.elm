module Settings.View exposing (view)

-- Core

import Html
import Html.Attributes as Attribute


-- Types

import Settings.Types as Settings exposing (Model, Msg)


-- Settings View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container-fluid" ]
        [ Html.br [] []
        , Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-3" ]
                [ settingsMenuList ]
            , Html.div [ Attribute.class "col-xs-9" ]
                [ settingsContent model ]
            ]
        ]



-- Settings Menu List


settingsMenuList : Html.Html Msg
settingsMenuList =
    Html.ul [ Attribute.class "nav nav-pills nav-stacked" ]
        [ Html.li [ Attribute.class "active" ]
            [ Html.a
                [ Attribute.href "#pill_content_1"
                , Attribute.attribute "data-toggle" "pill"
                ]
                [ Html.text "Home" ]
            ]
        , Html.li []
            [ Html.a
                [ Attribute.href "#pill_content_2"
                , Attribute.attribute "data-toggle" "pill"
                ]
                [ Html.text "Second" ]
            ]
        , Html.li []
            [ Html.a
                [ Attribute.href "#pill_content_3"
                , Attribute.attribute "data-toggle" "pill"
                ]
                [ Html.text "Third" ]
            ]
        ]



-- Settings Content


settingsContent : Model -> Html.Html Msg
settingsContent model =
    Html.div [ Attribute.class "tab-content" ]
        [ Html.div
            [ Attribute.id "pill_content_1"
            , Attribute.class "tab-pane fade in active"
            ]
            [ Html.text "First" ]
        , Html.div
            [ Attribute.id "pill_content_2"
            , Attribute.class "tab-pane fade"
            ]
            [ Html.text "Second" ]
        , Html.div
            [ Attribute.id "pill_content_3"
            , Attribute.class "tab-pane fade"
            ]
            [ Html.text "Third" ]
        ]
