module Mailbox.Preview.Types exposing (..)

-- Types

import Editor.Types


-- Mailbox Preview Messages


type Msg
    = RemoveMessage String
    | EditorMessages Editor.Types.Msg



-- Mailbox Preview Model Schema


type alias Model =
    { selectedMessageId : String
    , editorModel : Editor.Types.Model
    }



-- Mailbox Preview Model


model : Model
model =
    { selectedMessageId = ""
    , editorModel = Editor.Types.model
    }
