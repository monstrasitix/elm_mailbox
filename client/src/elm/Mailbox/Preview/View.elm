module Mailbox.Preview.View exposing (view)

-- Core

import Date
import Html
import Html.Events as Event
import Html.Attributes as Attribute
import Date.Extra.Format exposing (format)
import Date.Extra.Config.Config_en_us exposing (config)


-- Types

import Mailbox.Preview.Types as Preview exposing (Model, Msg)


-- Views

import Editor.View


-- Models

import Model.Message as Message
import Model.Contact as Contact


-- Mailbox Preview View Function


view : Model -> Message.Message -> Html.Html Msg
view model message =
    Html.div
        [ Attribute.class "panel panel-default contact-list scrollable" ]
        [ Html.div [ Attribute.class "panel-body" ]
            [ Html.div [ Attribute.class "row" ]
                [ Html.div [ Attribute.class "col-xs-12" ]
                    [ previewHeader message
                    , Html.br [] []
                    , previewInformation message
                    ]
                ]
            , Html.hr [] []
            , Html.div [ Attribute.class "row" ]
                [ Html.div [ Attribute.class "col-xs-12" ]
                    [ Html.iframe
                        [ Attribute.srcdoc message.body
                        , Attribute.class "message-iframe"
                        , Attribute.seamless True
                        , Attribute.attribute "onload" "iframeLoaded(this)"
                        ]
                        []
                    ]
                ]
            , Html.hr [] []
            ]
        , Editor.View.view
            [ Attribute.id "replyTextarea"
            , Attribute.class "contact-list textarea"
            , Attribute.style [ ( "margin-top", "0" ), ( "width", "100px" ), ( "min-height", "200px" ) ]
            ]
            model.editorModel
            |> Html.map Preview.EditorMessages
        , Editor.View.footer
            |> Html.map Preview.EditorMessages
        ]



-- Preview Information


previewInformation : Message.Message -> Html.Html Msg
previewInformation message =
    Html.div
        [ Attribute.class "row" ]
        [ Html.div [ Attribute.class "col-xs-6" ]
            [ Html.p [ Attribute.class "text-muted" ]
                (previewFrom message.from)
            ]
        , Html.div [ Attribute.class "col-xs-6" ]
            [ Html.p [ Attribute.class "text-muted pull-right" ]
                [ Html.text <| previewDate message.date ]
            ]
        ]



-- Preview Header


previewHeader : Message.Message -> Html.Html Msg
previewHeader message =
    Html.div [ Attribute.class "row" ]
        [ Html.div [ Attribute.class "col-xs-6" ]
            [ Html.p []
                [ Html.strong []
                    [ Html.text message.subject ]
                ]
            ]
        , Html.div [ Attribute.class "col-xs-6" ]
            [ Html.div [ Attribute.class "btn-group pull-right" ]
                [ Html.button
                    [ Attribute.type_ "button"
                    , Attribute.class "btn btn-sm btn-primary"
                    , Event.onClick <| Preview.RemoveMessage message.id
                    ]
                    [ Html.span []
                        [ Html.i [ Attribute.class "fa fa-angle-right" ] [] ]
                    ]
                ]
            , Html.div [ Attribute.class "clearfix" ] []
            ]
        ]



-- Preview From


previewFrom : List Contact.Contact -> List (Html.Html Msg)
previewFrom contacts =
    case List.head contacts of
        Just contact ->
            [ Html.p [] [ Html.text <| "From: " ++ contact.name ]
            , Html.p [] [ Html.text <| "Email: " ++ contact.email ]
            ]

        Nothing ->
            [ Html.text "Unknown" ]



-- Preview Date


previewDate : Int -> String
previewDate timestamp =
    let
        date : Date.Date
        date =
            Date.fromTime <| toFloat (timestamp * 1000)

        formated : String
        formated =
            format config "%m/%d/%Y" date
    in
        formated
