module Mailbox.Preview.Subscriptions exposing (..)

-- Types

import Mailbox.Preview.Types as Preview exposing (Model, Msg)


-- Subscriptions

import Editor.Subscriptions as Editor


-- Mailbox Preview Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    Editor.subscriptions model.editorModel
        |> Sub.map Preview.EditorMessages
