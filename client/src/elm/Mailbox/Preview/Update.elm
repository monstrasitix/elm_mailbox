module Mailbox.Preview.Update exposing (..)

-- Types

import Mailbox.Preview.Types as Preview exposing (Model, Msg)


-- Updates

import Editor.Update


-- Mailbox Preview Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Removing Message
        Preview.RemoveMessage messageId ->
            (!)
                (model
                    |> updateSelectedMessageId messageId
                )
                []

        -- Editor Messages
        Preview.EditorMessages message ->
            let
                ( newModel, newCmd ) =
                    Editor.Update.update message model.editorModel
            in
                (!)
                    { model | editorModel = newModel }
                    [ Cmd.map Preview.EditorMessages newCmd ]


updateSelectedMessageId : String -> Model -> Model
updateSelectedMessageId selectedMessageId model =
    { model | selectedMessageId = selectedMessageId }
