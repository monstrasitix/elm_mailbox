module Mailbox.List.View exposing (view)

-- TODO: Refactor mailbox list view
-- Core

import Html
import Date
import Regex
import Html.Events as Event
import Html.Attributes as Attribute
import Date.Extra.Format exposing (format)
import Date.Extra.Config.Config_en_us exposing (config)


-- Types

import Mailbox.List.Types as List exposing (Model, Msg)


-- Models

import Model.Message as Message
import Model.Contact as Contact


-- Helpers

import Helper.Progress as Progress


-- Mailbox List View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "panel panel-default contact-list" ]
        [ mailboxSearch model.searchValue model.unreadFilter
        , if model.requestingMessages then
            Html.div [ Attribute.class "panel-body" ]
                [ Progress.progressBar 100
                ]
          else
            mailboxList model
        ]



-- Message List


mailboxList : Model -> Html.Html Msg
mailboxList { messages, searchValue, unreadFilter } =
    let
        regex : Regex.Regex
        regex =
            Regex.regex searchValue
                |> Regex.caseInsensitive

        filteredMessages : List Message.Message
        filteredMessages =
            messages
                |> (\messages ->
                        case unreadFilter of
                            True ->
                                List.filter (\message -> message.unread) messages

                            False ->
                                messages
                   )
                |> List.filter
                    (\message ->
                        String.isEmpty searchValue
                            || Regex.contains regex message.subject
                            || filterByContact regex message
                    )
    in
        case List.isEmpty filteredMessages of
            False ->
                Html.ul [ Attribute.class "list-group scrollable" ]
                    (List.map mailboxMessage filteredMessages)

            True ->
                Html.div [ Attribute.class "panel-body" ]
                    [ Html.div [ Attribute.class "well well-lg text-center" ]
                        [ Html.text "No messages"
                        ]
                    ]



-- Filtering By Contact's Name And Email


filterByContact : Regex.Regex -> Message.Message -> Bool
filterByContact regex message =
    case List.head message.from of
        Just contact ->
            Regex.contains regex contact.name
                || Regex.contains regex contact.email

        Nothing ->
            False



-- Message


mailboxMessage : Message.Message -> Html.Html Msg
mailboxMessage message =
    Html.li
        [ Attribute.class "list-group-item list-group-item-hover"
        , Attribute.classList
            [ ( "message-unread", message.unread )
            ]
        , Event.onClick <| List.UpdateSelectedMessage message.id
        ]
        [ mailboxMessageFrom message.from
        , mailboxMessageDate message.date
        , Html.div [ Attribute.class "clearfix" ] []
        , Html.div []
            [ Html.span [ Attribute.class "text-muted text-truncate" ]
                [ Html.text message.subject ]
            , Html.span [ Attribute.class "pull-right" ]
                [ Html.i
                    [ Attribute.class "fa"
                    , Attribute.classList
                        [ ( "fa-star", message.starred )
                        , ( "fa-star-o", not message.starred )
                        ]
                    ]
                    []
                ]
            ]
        ]



-- Message From


mailboxMessageFrom : List Contact.Contact -> Html.Html Msg
mailboxMessageFrom contacts =
    case List.head contacts of
        Just contact ->
            Html.span []
                [ Html.strong [] [ Html.text contact.name ]
                ]

        Nothing ->
            Html.text ""



-- Message Date


mailboxMessageDate : Int -> Html.Html Msg
mailboxMessageDate timestamp =
    let
        date : Date.Date
        date =
            Date.fromTime <| toFloat (timestamp * 1000)

        formated : String
        formated =
            format config "%m/%d/%Y" date
    in
        Html.span [ Attribute.class "pull-right" ]
            [ Html.text formated ]



-- Mailbox Search Field


mailboxSearch : String -> Bool -> Html.Html Msg
mailboxSearch searchValue unreadFilter =
    Html.div [ Attribute.class "panel-body" ]
        [ Html.div [ Attribute.class "input-group" ]
            [ Html.span [ Attribute.class "input-group-addon" ]
                [ Html.i [ Attribute.class "fa fa-search" ] [] ]
            , Html.input
                [ Attribute.type_ "text"
                , Attribute.value searchValue
                , Attribute.class "form-control"
                , Attribute.placeholder "Search messages"
                , Event.onInput List.UpdateSearchValue
                ]
                []
            , Html.span [ Attribute.class "input-group-btn" ]
                [ Html.button
                    [ Attribute.type_ "button"
                    , Attribute.class "btn"
                    , Attribute.classList
                        [ ( "btn-success", not unreadFilter )
                        , ( "btn-primary", unreadFilter )
                        ]
                    , Event.onClick List.UpdateUnreadFilter
                    ]
                    [ Html.i
                        [ Attribute.class "fa fa-envelope-o"
                        , Attribute.classList
                            [ ( "fa-envelope-o", not unreadFilter )
                            , ( "fa-envelope-open-o", unreadFilter )
                            ]
                        ]
                        []
                    ]
                ]
            ]
        ]
