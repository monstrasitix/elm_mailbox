module Mailbox.List.Types exposing (..)

-- Core

import Http


-- Models

import Model.Message as Message
import Model.Response as Response


-- Mailbox List Messages


type Msg
    = UpdateUnreadFilter
    | UpdateSearchValue String
    | UpdateSelectedMessage String
    | ResponseMessages (Result Http.Error (Response.Success (List Message.Message)))



-- Mailbox List Model Schema


type alias Model =
    { unreadFilter : Bool
    , searchValue : String
    , requestingMessages : Bool
    , selectedMessage : Maybe String
    , messages : List Message.Message
    }



-- Mailbox List Model


model : Model
model =
    { messages = []
    , searchValue = ""
    , unreadFilter = False
    , requestingMessages = True
    , selectedMessage = Nothing
    }
