module Mailbox.List.Update exposing (update)

-- Core

import Mailbox.List.Types as List exposing (Model, Msg)


-- Models

import Model.Message as Message


-- Mailbox List Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- List Of Messages Response Success
        List.ResponseMessages (Ok { data, status }) ->
            (!)
                (model
                    |> updateMessages data
                    |> updateRequestingMessages (not status)
                )
                []

        -- List Of Messages Response Failure
        List.ResponseMessages (Err error) ->
            model ! []

        -- Updating Value From Search Input Field
        List.UpdateSearchValue newValue ->
            (!)
                (model |> updateSearchField newValue)
                []

        -- Update Unread Filter
        List.UpdateUnreadFilter ->
            (!)
                (model |> updareUnreadFilter (not model.unreadFilter))
                []

        -- Selects Message's ID
        List.UpdateSelectedMessage messageId ->
            (!)
                (model |> updateSelectedMessage (Just messageId))
                []


updateMessages : List Message.Message -> Model -> Model
updateMessages messages model =
    { model | messages = messages }


updateRequestingMessages : Bool -> Model -> Model
updateRequestingMessages requestingMessages model =
    { model | requestingMessages = requestingMessages }


updateSearchField : String -> Model -> Model
updateSearchField searchValue model =
    { model | searchValue = searchValue }


updareUnreadFilter : Bool -> Model -> Model
updareUnreadFilter unreadFilter model =
    { model | unreadFilter = unreadFilter }


updateSelectedMessage : Maybe String -> Model -> Model
updateSelectedMessage selectedMessage model =
    { model | selectedMessage = selectedMessage }
