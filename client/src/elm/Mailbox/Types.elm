module Mailbox.Types exposing (..)

-- Types

import Mailbox.List.Types as List
import Mailbox.Preview.Types as Preview


-- Mailbox Messages


type Msg
    = ListMessages List.Msg
    | PreviewMessages Preview.Msg



-- Mailbox Model Schema


type alias Model =
    { listModel : List.Model
    , previewModel : Preview.Model
    }



-- Mailbox Model


model : Model
model =
    { listModel = List.model
    , previewModel = Preview.model
    }
