module Mailbox.View exposing (..)

-- Core

import Mailbox.Types as Mailbox exposing (Model, Msg)
import Html
import Html.Attributes as Attribute


-- Views

import Mailbox.List.View as List
import Mailbox.Preview.View as Preview


-- Models

import Model.Message as Message


-- Mailbox View Function


view : Model -> Html.Html Msg
view model =
    case model.listModel.selectedMessage of
        Just messageId ->
            let
                message : Maybe Message.Message
                message =
                    model.listModel.messages
                        |> List.filter (\message -> message.id == messageId)
                        |> List.head
            in
                case message of
                    Just message ->
                        mailboxListWithPreview model message

                    Nothing ->
                        mailboxMessageNotFound

        Nothing ->
            mailboxList model



-- Mailbox List


mailboxList : Model -> Html.Html Msg
mailboxList model =
    Html.div [ Attribute.class "container-fluid" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-12" ]
                [ List.view model.listModel
                    |> Html.map Mailbox.ListMessages
                ]
            ]
        ]



-- Mailbox List With Preview


mailboxListWithPreview : Model -> Message.Message -> Html.Html Msg
mailboxListWithPreview model message =
    Html.div [ Attribute.class "container-fluid" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-6" ]
                [ List.view model.listModel
                    |> Html.map Mailbox.ListMessages
                ]
            , Html.div [ Attribute.class "col-xs-6" ]
                [ Preview.view model.previewModel message
                    |> Html.map Mailbox.PreviewMessages
                ]
            ]
        ]



-- No Message's Id


mailboxMessageNotFound : Html.Html Msg
mailboxMessageNotFound =
    Html.div [ Attribute.class "panel-body" ]
        [ Html.div [ Attribute.class "well well-lg text-center" ]
            [ Html.text "Message was not found" ]
        ]
