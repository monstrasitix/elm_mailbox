module Mailbox.Subscriptions exposing (..)

-- Types

import Mailbox.Types as Mailbox exposing (Model, Msg)


-- Subscriptions

import Mailbox.Preview.Subscriptions


-- Mailbox Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Mailbox.Preview.Subscriptions.subscriptions model.previewModel
            |> Sub.map Mailbox.PreviewMessages
        ]
