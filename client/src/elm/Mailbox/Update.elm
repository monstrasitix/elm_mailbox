module Mailbox.Update exposing (update)

-- Core

import Mailbox.Types as Mailbox exposing (Model, Msg)


-- Updates

import Mailbox.List.Update as List
import Mailbox.Preview.Update as Preview


-- Mailbox Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Binding Mailbox List
        Mailbox.ListMessages message ->
            let
                ( newModel, newCmd ) =
                    List.update message model.listModel
            in
                (!)
                    { model | listModel = newModel }
                    [ Cmd.map Mailbox.ListMessages newCmd ]

        -- Binding Mailbox Preview
        Mailbox.PreviewMessages message ->
            let
                ( newModel, newCmd ) =
                    Preview.update message model.previewModel

                updateListModel listModel =
                    { listModel | selectedMessage = Nothing }

                newerModel : Model
                newerModel =
                    if Just model.previewModel.selectedMessageId /= model.listModel.selectedMessage then
                        { model | listModel = updateListModel model.listModel }
                    else
                        model
            in
                (!)
                    { newerModel | previewModel = newModel }
                    [ Cmd.map Mailbox.PreviewMessages newCmd ]
