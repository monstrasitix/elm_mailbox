module Error.View exposing (..)

-- Core

import Html
import Html.Attributes as Attribute


-- Not Found Route Function


notFoundRoute : Html.Html message
notFoundRoute =
    Html.div [ Attribute.class "well well-lg text-center" ]
        [ Html.text "Route not found"
        ]
