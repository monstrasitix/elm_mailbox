module Composition.Subscriptions exposing (..)

-- Types

import Composition.Types as Composition exposing (Model, Msg)


-- Subscriptions

import Editor.Subscriptions as Editor


-- Composition Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    Editor.subscriptions model.editorModel
        |> Sub.map Composition.EditorMessages
