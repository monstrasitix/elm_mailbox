module Composition.Types exposing (..)

-- Types

import Editor.Types as Editor


-- Composition Messages


type Msg
    = EditorMessages Editor.Msg



-- Composition Model Schema


type alias Model =
    { editorModel : Editor.Model
    }



-- Composition Model


model : Model
model =
    { editorModel = Editor.model
    }
