module Composition.Update exposing (update)

-- Types

import Composition.Types as Composition exposing (Model, Msg)


-- Updates

import Editor.Update as Editor


-- Composition Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Editor Messages
        Composition.EditorMessages message ->
            let
                ( newModel, newCmd ) =
                    Editor.update message model.editorModel
            in
                (!)
                    { model | editorModel = newModel }
                    [ Cmd.map Composition.EditorMessages newCmd ]
