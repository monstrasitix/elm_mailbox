module Composition.View exposing (view)

-- Core

import Html
import Html.Attributes as Attribute


-- Types

import Composition.Types as Data exposing (Model, Msg)


-- Views

import Editor.View as Editor


-- Composition View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container-fluid" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-8" ]
                [ compositionForm model
                ]
            , Html.div [ Attribute.class "col-xs-4" ]
                [ contactList
                ]
            ]
        ]



-- Composition Contact List


contactList : Html.Html Msg
contactList =
    Html.div [ Attribute.class "panel panel-default contact-list" ]
        [ Html.div [ Attribute.class "panel-body" ]
            [ Html.h3 [] [ Html.text "Contacts" ]
            ]
        , Html.ul [ Attribute.class "list-group scrollable" ]
            (List.range 1 31 |> List.map (\n -> contactListItem))
        ]



-- Composition Contact Image


contactListItemImage : Html.Html Msg
contactListItemImage =
    Html.div [ Attribute.class "contact" ]
        [ Html.img
            [ Attribute.class "img-circle contact-image"
            , Attribute.src "https://static1.squarespace.com/static/50de3e1fe4b0a05702aa9cda/t/50eb2245e4b0404f3771bbcb/1357589992287/ss_profile.jpg"
            ]
            []
        , Html.div [ Attribute.class "contact-status" ] []
        ]



-- Composition Contact Information


contactListItemInformation : Html.Html Msg
contactListItemInformation =
    Html.div [ Attribute.class "media-body" ]
        [ Html.h4 [ Attribute.class "media-heading" ]
            [ Html.text "John Doe" ]
        , Html.p [ Attribute.class "text-muted" ]
            [ Html.text "someone@mail.com" ]
        , Html.div [ Attribute.class "btn-group pull-right" ]
            [ Html.button
                [ Attribute.type_ "button"
                , Attribute.class "btn btn-success btn-xs"
                ]
                [ Html.text "To" ]
            , Html.button
                [ Attribute.type_ "button"
                , Attribute.class "btn btn-success btn-xs"
                ]
                [ Html.text "Bcc" ]
            , Html.button
                [ Attribute.type_ "button"
                , Attribute.class "btn btn-success btn-xs"
                ]
                [ Html.text "Cc" ]
            ]
        ]



-- Composition Contact List Item


contactListItem : Html.Html Msg
contactListItem =
    Html.li [ Attribute.class "list-group-item list-group-item-hover" ]
        [ Html.div [ Attribute.class "media" ]
            [ Html.div [ Attribute.class "media-left media-middle" ]
                [ contactListItemImage ]
            , contactListItemInformation
            ]
        ]



-- Composition Initial Fields


compositionInitialFields : Html.Html Msg
compositionInitialFields =
    Html.div [ Attribute.class "panel-body" ]
        [ mainFields ]



-- Composition Optional Fields


compositionOptionalFields : Html.Html Msg
compositionOptionalFields =
    Html.div
        [ Attribute.class "collapse"
        , Attribute.id "compositionFormMoreFields"
        ]
        [ Html.div [ Attribute.class "well no-margin-bottom" ]
            [ fieldBcc, fieldCc ]
        ]



-- Composition Form


compositionForm : Model -> Html.Html Msg
compositionForm model =
    Html.div
        [ Attribute.class "panel panel-default contact-list" ]
        [ Html.div [ Attribute.class "panel-heading" ]
            [ Html.button
                [ Attribute.attribute "data-toggle" "collapse"
                , Attribute.class "btn btn-primary btn-sm pull-right"
                , Attribute.attribute "data-target" "#compositionFormMoreFields"
                ]
                [ Html.text "More fields" ]
            ]
        , Html.form [ Attribute.class "flex flex-direction-column contact-form" ]
            [ compositionInitialFields
            , compositionOptionalFields
            , Editor.view
                [ Attribute.id "compositionTextarea"
                , Attribute.class "contact-list textarea"
                , Attribute.style [ ( "margin-top", "0" ), ( "width", "100px" ) ]
                ]
                model.editorModel
                |> Html.map Data.EditorMessages
            , Editor.footer
                |> Html.map Data.EditorMessages
            ]
        ]



-- MainFields


mainFields : Html.Html Msg
mainFields =
    Html.div [ Attribute.class "form-group" ]
        [ Html.div [ Attribute.class "col-xs-6" ]
            [ Html.input
                [ Attribute.class "form-control"
                , Attribute.type_ "text"
                , Attribute.id "compositionTo"
                , Attribute.placeholder "To"
                ]
                []
            ]
        , Html.div [ Attribute.class "col-xs-6" ]
            [ Html.input
                [ Attribute.class "form-control"
                , Attribute.type_ "text"
                , Attribute.id "compositionSubject"
                , Attribute.placeholder "Subject"
                ]
                []
            ]
        ]



-- Field Bcc


fieldBcc : Html.Html Msg
fieldBcc =
    Html.div [ Attribute.class "form-group" ]
        [ Html.input
            [ Attribute.class "form-control"
            , Attribute.type_ "text"
            , Attribute.id "compositionBcc"
            , Attribute.placeholder "Bcc"
            ]
            []
        ]



-- Field Cc


fieldCc : Html.Html Msg
fieldCc =
    Html.div [ Attribute.class "form-group" ]
        [ Html.input
            [ Attribute.class "form-control"
            , Attribute.type_ "text"
            , Attribute.id "compositionCc"
            , Attribute.placeholder "Cc"
            ]
            []
        ]
