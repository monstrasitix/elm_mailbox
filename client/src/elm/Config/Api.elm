module Config.Api exposing (..)

-- Config API Model Schema


type alias Config =
    { uri : String
    , nylasToken : String
    }



-- Config API Model


config : Config
config =
    { uri = "http://localhost:8000/api/mailbox/v0"
    , nylasToken = "9OqWAkz4eIKxBsYP9zvOD3sW70Nll2"
    }
