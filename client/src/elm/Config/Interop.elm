module Config.Interop exposing (..)

-- Config Interop Model Schema


type alias Config =
    { observableSelector : String
    }



-- Config Interpo Model


config : Config
config =
    { observableSelector = "routing"
    }
