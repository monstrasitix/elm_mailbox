import $ from 'jquery';
import Config from './config';
import Elm from '../elm/Main.elm';
import QuillWrapper from './libs/quill';

const app = Elm.Main.embed(document.querySelector(Config.appSelector));

let observer;

app.ports.selectors.subscribe((identifier) => {
    observer = new MutationObserver((mutations) => {
        Config.editors.forEach((editor) => {
            if ($(mutations[0].target).find(editor.selector).length) {
                const quillEditor = new QuillWrapper(
                    editor.selector,
                    editor.config
                );
                quillEditor.init();
                quillEditor.onChange(app.ports);
            }
        });
    });

    observer.observe(document.getElementById(identifier), {
        subtree: true,
        childList: true,
    });
});

$(() => {
    $("[data-toggle='toggle']").on('click', function () {
        var selector = $(this).data("target");
        $(selector).toggleClass('in');
    });
});