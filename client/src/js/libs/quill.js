import Quill from 'quill';

class QuillWrapper {
    constructor(selector, configuration) {
        this.theme = 'snow';
        this.selector = selector;
        this.configuration = configuration

        this.init = this.init.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    init() {
        this.instance = new Quill(this.selector, {
            modules: {
                toolbar: this.toolbar,
            },
            theme: this.theme,
            placeholder: this.configuration.placeholder,
        });
    }

    onChange(ports) {
        this.instance.on('text-change', () => {
            const value = this.instance.container.firstChild.innerHTML;
            ports.editorReceiveValue.send(value);
        });
    }

    get toolbar() {
        return [
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ font: [] }],
            ['bold', 'italic', 'underline', 'strike'],
            [{ list: 'ordered' }, { list: 'bullet' }],
            [{ indent: '-1' }, { indent: '+1' }],
            [{ color: [] }, { background: [] }],
            [{ align: [] }],
        ];
    }
}

export default QuillWrapper;