export default {
    appSelector: '#app',
    editors: [
        {
            selector: '#replyTextarea',
            config: {
                placeholder: 'Reply ...',
            },
        },
        {
            selector: '#compositionTextarea',
            config: {
                placeholder: 'Message ...',
            },
        }
    ],
};